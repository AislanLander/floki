import React, { useState, useEffect } from "react";
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TextInput,
  Text,
} from "react-native";
import { HeaderComponent } from "../../components/header/header";
import { OptionList } from "../../components/optionList/optionList";
import { Header, Overlay } from "react-native-elements";
import { Colors } from "../../globals";
import Spinner from "react-native-loading-spinner-overlay";

const categorias = [
  {
    id: "1",
    title: "Frutas",
    quantidade: 0,

    itens: [
      {
        nome: "maça",
        quantidade: 0,

        un: "kg",
      },
      {
        nome: "pera",
        quantidade: 0,
        un: "kg",
      },
    ],
  },
  {
    id: "2",
    title: "Bebidas",
    quantidade: 0,

    itens: [
      {
        nome: "suco",
        quantidade: 0,

        un: "un",
      },
      {
        nome: "cerveja",
        quantidade: 0,

        un: "un",
      },
    ],
  },
  {
    id: "3",
    title: "Carnes",
    quantidade: 0,

    itens: [
      {
        nome: "frango",
        quantidade: 0,

        un: "kg",
      },
      {
        nome: "bife",
        quantidade: 0,

        un: "kg",
      },
    ],
  },
  {
    id: "5",
    title: "Padaria",
    quantidade: 0,

    itens: [
      {
        nome: "massa",
        quantidade: 0,

        un: "kg",
      },
      {
        nome: "pao",
        quantidade: 0,

        un: "kg",
      },
    ],
  },
  {
    id: "4",
    title: "Limpeza",
    quantidade: 0,
    itens: [
      {
        nome: "detergente",
        quantidade: 0,

        un: "un",
      },
      {
        nome: "limpador",
        quantidade: 0,
        un: "un",
      },
    ],
  },
];
const HomeScreen = (props) => {
  const [selected, setSelected] = useState(null);
  const [value, setValue] = useState(0);
  const [spinner, setSpinner] = useState(false);
  const [visible, setVisible] = useState(false);

  const [categoriasState, setCategoriasState] = useState([...categorias]);
  useEffect(() => {
    selected && setValue(selected.price);
  }, [selected]);
  const reset = () => {
    let tempArray = [...categorias];
    tempArray.map((categoria, index) => {
      categoria.itens.map((subItem, secondaryIndex) => {
        subItem.quantidade = 0;
      });
    });
    setCategoriasState(tempArray);
  };
  const atualizarCategorias = (newCategorias) => {
    newCategorias.forEach((categoria) => {
      let quantidadeItensCategoria = 0;
      categoria.itens.forEach((item) => {
        quantidadeItensCategoria += item.quantidade;
      });
      categoria.quantidade = quantidadeItensCategoria;
    });
    setCategoriasState([...newCategorias]);
  };
  const criarPedido = () => {
    setSpinner(true);
    setTimeout(() => {
      setSpinner(false);
    }, 2000);
  };
  const toggleOverlay = () => {
    setVisible(!visible);
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Spinner
        visible={spinner}
        textContent={"Realizando Pedido..."}
        textStyle={{ color: "#FFF" }}
      />
      <Overlay
        isVisible={visible}
        onBackdropPress={toggleOverlay}
        overlayStyle={{ width: 300, height: 300, borderRadius: 6 }}
      >
        <Text>Observações</Text>
        <TextInput>!</TextInput>
      </Overlay>
      <Header
        placement="left"
        leftComponent={{ icon: "menu", color: "#fff" }}
        backgroundColor={Colors.blue}
        centerComponent={{
          text: "Bem vinda Mariana",
          style: { color: "#fff" },
        }}
        rightComponent={{ icon: "home", color: "#fff" }}
      />
      <ScrollView contentContainerStyle={{ paddingBottom: 40 }}>
        <HeaderComponent
          value={value}
          categorias={[...categoriasState]}
          reset={reset}
          criarPedido={criarPedido}
          toggleOverlay={toggleOverlay}
        />
        <OptionList items={[...categoriasState]} action={atualizarCategorias} />
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: "blue",
    height: 60,
    justifyContent: "center",
  },
});
export default HomeScreen;
