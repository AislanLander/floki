import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  FlatList,
  View,
  Text,
  Image,
  subTitleStyle,
  TouchableOpacity,
} from "react-native";
import { Colors } from "../../globals";
import { Icon, SearchBar } from "react-native-elements";
import { grayText } from "../../globals/colors";
import { ButtonComponent } from "../mainButton/button";

export const OptionList = (props) => {
  const { container, itemStyle, titleStyle, selectedStyle } = styles;
  const [items, setItems] = useState(props.items);
  const [search, setSearch] = useState("");
  const [categoriaSelecionada, setCategoriaSelecionada] = useState(0);
  const alterarItens = (item, index, quantidade) => {
    if (quantidade < 0) return null;
    let tempItems = [...items];
    tempItems[categoriaSelecionada].itens[index].quantidade = quantidade;
    setItems([...tempItems]);
  };

  useEffect(() => {
    setItems(props.items);
  }, [props.items]);

  function RenderItem({ id, item, title, price, img, index }) {
    return (
      <TouchableOpacity onPress={() => setCategoriaSelecionada(index)}>
        <View
          style={[
            itemStyle,
            props.selected && props.selected.id === id ? selectedStyle : null,
            item && item.selected ? selectedStyle : null,
          ]}
        >
          <Text style={titleStyle}>{title}</Text>

          {img && (
            <Image
              style={{ margin: 0, width: 80, height: 80 }}
              resizeMode="contain"
              source={img}
            />
          )}
        </View>
      </TouchableOpacity>
    );
  }
  return (
    <View style={container}>
      <View style={styles.card}>
        <View style={styles.cardHeader}>
          <Icon
            name="md-calendar"
            type="ionicon"
            size={26}
            style={{ paddingLeft: 20, paddingRight: 20 }}
          />
          <Text style={{ fontWeight: "bold", color: grayText, fontSize: 20 }}>
            Adicionar Itens
          </Text>
        </View>
        <SearchBar
          lightTheme
          platform={"android"}
          containerStyle={{ backgroundColor: "#fff", borderWidth: 0 }}
          placeholder="Pesquisar itens..."
          //    onChangeText={updateSearch}
          value={search}
        />
        <FlatList
          data={items}
          horizontal
          renderItem={({ item, index }) => (
            <RenderItem
              id={item.id}
              item={item}
              title={item.title}
              price={item.price}
              img={item.image}
              index={index}
            />
          )}
          keyExtractor={(item) => item.id}
        />
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              paddingTop: 10,
              paddingBottom: 15,
            }}
          >
            <Text style={{ fontSize: 16 }}>Items</Text>

            <Text style={{ fontSize: 16 }}>Quantidade</Text>
          </View>
          <FlatList
            data={items[categoriaSelecionada].itens}
            renderItem={({ item, index }) => {
              return (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  <Text style={{ fontSize: 16 }}>{item.nome}</Text>
                  <View style={{ flexDirection: "row" }}>
                    <Icon
                      name="md-remove"
                      type="ionicon"
                      size={26}
                      style={{ paddingLeft: 20, paddingRight: 20 }}
                      onPress={() =>
                        alterarItens(item, index, item.quantidade - 1)
                      }
                    />
                    <Text style={{ fontSize: 16 }}>{item.quantidade}</Text>
                    <Icon
                      name="md-add"
                      type="ionicon"
                      size={26}
                      onPress={() =>
                        alterarItens(item, index, item.quantidade + 1)
                      }
                      style={{ paddingLeft: 5, paddingRight: 20 }}
                    />
                    <Text style={{ paddingLeft: 5, fontSize: 16 }}>
                      {item.un}
                    </Text>
                  </View>
                </View>
              );
            }}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
      <ButtonComponent
        onPress={() => {
          console.log(items);
          props.action(items);
        }}
        label="Adicionar ao carrinho"
        disabled={false}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, padding: 24 },
  card: {
    backgroundColor: "#fff",
    //    height: 250,
    width: "100%",
    borderRadius: 10,
    elevation: 6,
    padding: 20,
    marginBottom: 25,
  },
  cardHeader: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  itemStyle: {
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    paddingTop: 15,
    padding: 6,
    marginVertical: 8,
    marginHorizontal: 16,
    flexDirection: "row",
    borderRadius: 8,
  },
  titleStyle: {
    fontSize: 20,
  },
  subTitleStyle: {
    fontSize: 26,
    color: Colors.materialGray,
    alignSelf: "center",
  },
  selectedStyle: {
    borderWidth: 1,
    borderColor: Colors.PrimaryColorMedium,
    shadowColor: Colors.PrimaryColorLight,
  },
});
