import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Platform,
  UIManager,
  LayoutAnimation,
} from "react-native";
import { Icon, Header, Button } from "react-native-elements";
import { Colors } from "../../globals";

export const HeaderComponent = (props) => {
  const [expanded, setExpanded] = useState(false);
  console.log(props);
  if (Platform.OS === "android") {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  const changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setExpanded(!expanded);
  };
  const renderItens = () => {
    return props.categorias.map((categoria, index) => {
      return categoria.itens.map((subItem, secondaryIndex) => {
        if (subItem.quantidade) {
          return (
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
              key={index + secondaryIndex}
            >
              <Text>{subItem.nome}</Text>
              <Text>{subItem.quantidade}</Text>
            </View>
          );
        } else return null;
      });
    });
  };
  const { container, text, resumoPedidoCard, firstRow } = styles;
  return (
    <View style={container}>
      <View style={{ width: "100%", padding: 24 }}>
        <View
          style={[
            resumoPedidoCard,
            { height: expanded ? null : null, overflow: "hidden" },
          ]}
        >
          <View style={firstRow}>
            <Icon name="md-cart" type="ionicon" size={36} />
            <View>
              <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                Carrinho Atualss
              </Text>
              <Text>Entrega 5 feita às 10:00</Text>
            </View>
            <Icon
              name="ios-expand"
              type="ionicon"
              size={36}
              onPress={changeLayout}
            />
          </View>
          {expanded ? (
            <FlatList
              data={props.categorias}
              keyExtractor={(item) => item.id + ""}
              numColumns={2} // Número de colunas
              renderItem={({ item }) => {
                return (
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Icon name="ios-beer" type="ionicon" size={26} />
                    <Text style={{ padding: 10 }}>{item.quantidade}</Text>
                    <Text>itens</Text>
                  </View>
                );
              }}
            />
          ) : (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  paddingTop: 10,
                  paddingBottom: 15,
                }}
              >
                <View>
                  <Text style={{ fontSize: 16 }}>Items</Text>
                </View>
                <View>
                  <Text style={{ fontSize: 16, alignSelf: "flex-end" }}>
                    Quantidade
                  </Text>

                  {/* <View style={{ flexDirection: "row" }}>
                    <Icon name="md-remove" type="ionicon" size={26} />
                    <Text
                      style={{ fontSize: 16, alignSelf: "center", padding: 2 }}
                    >
                      6
                    </Text>
                    <Icon name="md-add" type="ionicon" size={26} />
                    <Text
                      style={{
                        paddingLeft: 5,
                        fontSize: 16,
                        alignSelf: "center",
                      }}
                    >
                      KG
                    </Text>
                  </View> */}
                </View>
              </View>
              <View>{renderItens()}</View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <Button
                  title="Fazer Pedido"
                  buttonStyle={{ width: "100%", backgroundColor: Colors.blue }}
                  onPress={() => props.criarPedido()}
                />
                <Icon
                  name="md-trash"
                  type="ionicon"
                  size={26}
                  color={Colors.blue}
                  onPress={() => props.reset()}
                />
                <Icon
                  name="md-timer"
                  type="ionicon"
                  size={26}
                  color={Colors.blue}
                />
                <Icon
                  name="md-create"
                  type="ionicon"
                  size={26}
                  color={Colors.blue}
                  onPress={() => props.toggleOverlay()}
                />
              </View>
            </View>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-between",
    //padding: 24,
    alignItems: "center",
  },
  text: {
    fontSize: 18,
    fontWeight: "bold",
  },

  resumoPedidoCard: {
    backgroundColor: "#fff",
    height: 250,
    width: "100%",
    borderRadius: 10,
    elevation: 6,
    padding: 20,
  },
  firstRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 20,
  },
});
